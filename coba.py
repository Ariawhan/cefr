# Fungsi untuk menghitung jarak euclidean antara dua vektor
def euclidean_distance(v1, v2):
    squared_distance = sum((x - y) ** 2 for x, y in zip(v1, v2))
    return squared_distance ** 0.5

# Data set n-gram dari kalimat pelatihan
ngram_corpus = [
    ["Produk ini", "ini sangat", "sangat bagus"],
    ["Pengalaman belanja", "belanja di", "di toko", "toko ini", "ini sangat", "sangat buruk"],
    ["Saya sangat", "sangat puas", "puas dengan", "dengan layanan", "layanan pelanggan", "pelanggan yang", "yang diberikan"],
    ["Kualitas produk", "produk mereka", "mereka mengecewakan"]
]

# Label sentimen
labels = ['positif', 'negatif', 'positif', 'negatif']

# Kalimat uji
test_ngram = ["Saya ingin", "ingin mengembalikan", "mengembalikan produk"]

# Hitung jarak Euclidean antara kalimat uji dan setiap kalimat dalam pelatihan
distances = [euclidean_distance(test_ngram, ngram) for ngram in ngram_corpus]

# Mengambil indeks kalimat terdekat (misalnya, 3 kalimat terdekat)
k = 3
nearest_indices = sorted(range(len(distances)), key=lambda i: distances[i])[:k]

# Melakukan klasifikasi berdasarkan mayoritas sentimen dari kalimat terdekat
nearest_labels = [labels[i] for i in nearest_indices]
predicted_sentiment = max(set(nearest_labels), key=nearest_labels.count)

print(f"Kalimat uji: {test_ngram}")
print(f"Sentimen yang diprediksi: {predicted_sentiment}")
