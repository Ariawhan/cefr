import pandas as pd
from Ngram import ngram
from WE import world_embedding
from Pos import Pos
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score

# Baca file CSV
file_path = "new_data.csv"  # Ganti dengan path file CSV Anda
data = pd.read_csv(file_path)

# print total dataset
print("Total dataset", len(data))

# Ambil kolom "answer" dan "label"
answers = data["answer"].tolist()
label = data["label"].tolist()


def extract_features(text1, text2):
    # Panjang Dokumen / hasil anggka
    doc_length_text = len(text1)
    doc_length_text2 = len(text2)

    doc_length = doc_length_text - doc_length_text2

    N_grams = ngram(text1, text2)  # Misalnya, hitung jumlah n-gram yang muncul dalam teks
    word_embedding = world_embedding(text1, text2)  # Misalnya, hitung world embedding dari teks
    pos_ngram = Pos(text1, text2)  # Misalnya, hitung POS n-gram dari teks

    # Jumlah karakter dalam teks
    char_count_text1 = len(text1)
    char_count_text2 = len(text2)

    char_diff = abs(char_count_text1-char_count_text2)

    # Jumlah kata dalam teks
    word_count_text1 = len(text1.split())
    word_count_text2 = len(text2.split())

    # Jumlah kata unik dalam teks
    unique_words_text1 = len(set(text1.split()))
    unique_words_text2 = len(set(text2.split()))

    # return doc_length, N_grams, word_embedding, pos_ngram, char_count_text1, char_count_text2, word_count_text1, word_count_text2, unique_words_text1, unique_words_text2
    return N_grams, word_embedding, pos_ngram, char_diff

# bagi data
X = answers
y = label

# Tentukan ukuran data testing (biasanya sekitar 20-30% dari seluruh data)
test_size = 0.11

# Melakukan pembagian data menjadi data training dan data testing
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size, random_state=42)

print ("Jlan sampk sini kok")

# Konversi teks menjadi fitur numerik
X_train_features = [extract_features(text1, text2) for text1, text2 in zip(X_train, y_train)]
X_test_features = [extract_features(text1, text2) for text1, text2 in zip(X_test, y_test)]

print("Total X_Train", len(X_train))
print("Total X_test", len(X_test))

# Simpan hasil ekstraksi fitur ke dalam DataFrame
data = {
    "N-Grams": [feat[0] for feat in X_train_features],
    "Word Embeddings": [feat[1] for feat in X_train_features],
    "PoS N-grams": [feat[2] for feat in X_train_features],
    "Char-diff": [feat[3] for feat in X_train_features],
}

df = pd.DataFrame(data)

# Export DataFrame ke file CSV
df.to_csv("hasil_ekstraksi_fitur_2.csv", index=False)



print ("Jlan sampk sini")

k=1

while True:
    # print("K:", k)
    if k>100:
        break
    # Inisialisasi dan latih model KNN
    knn_model = KNeighborsClassifier(n_neighbors=k)
    knn_model.fit(X_train_features, y_train)

    # Lakukan prediksi pada data testing
    y_pred = knn_model.predict(X_test_features)

    # Evaluasi kinerja model
    accuracy = accuracy_score(y_test, y_pred)
    precision = precision_score(y_test, y_pred, average='micro')  # Ubah average menjadi 'micro'
    recall = recall_score(y_test, y_pred, average='micro')  # Ubah average menjadi 'micro'

    print(f"k || {k} || ", accuracy)
    # print("KNN Precision:", precision)
    # print("KNN Recall:", recall)
    k += 2