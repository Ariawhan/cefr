import nltk
from nltk import pos_tag
from nltk import ngrams
from nltk.tokenize import word_tokenize
import numpy as np

def get_pos_tag_ngrams(text, n):
    words = word_tokenize(text)
    pos_tags = pos_tag(words)
    pos_tag_ngrams = list(ngrams(pos_tags, n))
    return pos_tag_ngrams

def document_vector(doc, all_ngrams):

    n_gram_size = 3

    # Get POS tag n-grams for the document
    doc_ngrams = set(get_pos_tag_ngrams(doc, n_gram_size))

    # Create a binary vector representation of the document
    vector = [1 if ngram in doc_ngrams else 0 for ngram in all_ngrams]
    
    return vector

def euclidean_distance(vector1, vector2):
    # Convert lists to NumPy arrays for faster computation
    np_vector1 = np.array(vector1)
    np_vector2 = np.array(vector2)

    # Calculate the Euclidean distance between the two vectors
    distance = np.linalg.norm(np_vector1 - np_vector2)

    return distance


def Pos(document1, document2):
    
    # ngram size
    n_gram_size = 3

    # Get POS tag n-grams for both documents
    all_ngrams = set(get_pos_tag_ngrams(document1, n_gram_size) + get_pos_tag_ngrams(document2, n_gram_size))

    # Create document vectors
    vector1 = document_vector(document1, all_ngrams)
    vector2 = document_vector(document2, all_ngrams)

    # Compare documents for plagiarism using Euclidean distance
    similarity_score = euclidean_distance(vector1, vector2)

    return similarity_score