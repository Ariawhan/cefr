import numpy as np
import nltk
from nltk.tokenize import word_tokenize

# Preprocessing
def preprocess(sentence):
    sentence = sentence.lower()
    words = word_tokenize(sentence)
    words = [word for word in words if word.isalnum()]
    return words

# Word Embeddings (dummy function, gunakan Word2Vec atau GloVe sesuai preferensi)
def word_embeddings(word):
    # Misalnya, kita gunakan vektor dummy dengan panjang 5
    return np.random.rand(5)

# Mencari vektor kalimat
def sentence_vector(sentence):
    words = preprocess(sentence)
    vectors = [word_embeddings(word) for word in words]
    return np.sum(vectors, axis=0)

# Perhitungan Jarak Euclidean Distance
def euclidean_distance(vector1, vector2):
    return np.linalg.norm(vector1 - vector2)


def world_embedding(document1, document2):

    vector1 = sentence_vector(document1)
    vector2 = sentence_vector(document2)
    
    distance = euclidean_distance(vector1, vector2)
    return distance
