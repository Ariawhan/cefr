import spacy
import numpy as np

def extract_dependency_triples(sentence):
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(sentence)
    triples = []
    for token in doc:
        if token.dep_ in ["nsubj", "dobj", "nsubjpass", "pobj"]:
            governor = token.head.text
            relation = token.dep_
            dependent = token.text
            triples.append((governor, relation, dependent))
    return triples

def vectorize_triples(triples):
    vocab = set(triples)
    vector = [1 if triple in triples else 0 for triple in vocab]
    return np.array(vector)

def euclidean_distance(vector1, vector2):
    return np.linalg.norm(vector1 - vector2)

def main():
    original_sentence = "The quick brown fox jumps over the lazy dog."
    suspicious_sentence = "The quick brown fox jumps over the lazy dog."

    original_triples = extract_dependency_triples(original_sentence)
    suspicious_triples = extract_dependency_triples(suspicious_sentence)

    original_vector = vectorize_triples(original_triples)
    suspicious_vector = vectorize_triples(suspicious_triples)

    similarity_score = euclidean_distance(original_vector, suspicious_vector)

    print(f"Original Sentence Triples: {original_triples}")
    print(f"Suspicious Sentence Triples: {suspicious_triples}")
    print(f"Similarity Score (Euclidean Distance): {similarity_score}")

    if similarity_score < 2.0:  # Adjust the threshold as needed
        print("The suspicious sentence may contain plagiarism.")
    else:
        print("The suspicious sentence is likely not plagiarized.")

if __name__ == "__main__":
    main()
