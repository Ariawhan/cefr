# Import pustaka yang diperlukan
from sklearn import datasets
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, classification_report

# Memuat dataset Iris
iris = datasets.load_iris()
X = iris.data
y = iris.target

# Membagi dataset menjadi data latih dan data uji
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)

# Fungsi untuk melatih dan evaluasi model KNN dengan berbagai nilai k
def evaluate_knn(k_values):
    for k in k_values:
        # Membuat model KNN dan melatihnya
        knn = KNeighborsClassifier(n_neighbors=k)
        knn.fit(X_train, y_train)

        # Memprediksi data uji
        y_pred = knn.predict(X_test)

        # Menghitung akurasi dan laporan klasifikasi
        accuracy = accuracy_score(y_test, y_pred)
        report = classification_report(y_test, y_pred, target_names=iris.target_names)

        print(f"Hasil untuk k = {k}:\n")
        print(f"Akurasi: {accuracy*100:.2f}%\n")
        print("Laporan Klasifikasi:")
        print(report)
        print('-' * 50)

# Nilai k yang akan dievaluasi
k_values = list(range(3, 11))

# Evaluasi untuk setiap nilai k
evaluate_knn(k_values)
