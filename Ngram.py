import numpy as np
from sklearn.feature_extraction.text import CountVectorizer

def get_ngram_representation(docs, n):
    vectorizer = CountVectorizer(ngram_range=(n, n))
    X = vectorizer.fit_transform(docs)
    return X.toarray()

def euclidean_distance(vector1, vector2):
    return np.sqrt(np.sum((vector1 - vector2) ** 2))


def ngram(document1, document2):

    texts = [document1, document2]

    # Hitung bigram (kata 2-gram)
    n = 2
    ngram_representation = get_ngram_representation(texts, n)

    # Dapatkan vektor representasi bigram untuk dua teks
    vector1 = ngram_representation[0]
    vector2 = ngram_representation[1]

    # Hitung jarak Euclidean antara dua teks
    distance = euclidean_distance(vector1, vector2)
    return distance
