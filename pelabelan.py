import csv


# Fungsi untuk mengkonversi grade ke label
def label_grade(grade):
    if 90 <= grade <= 100:
        return "C1"
    elif 80 <= grade <= 89:
        return "C2"
    elif 60 <= grade <= 79:
        return "B1"
    elif 40 <= grade <= 59:
        return "B2"
    elif 30 <= grade <= 39:
        return "A2"
    elif 0 <= grade <= 29:
        return "A1"


# Membaca data dari file CSV Anda
with open("new_dataset.csv", "r") as file:
    reader = csv.reader(file)
    header = next(reader)  # Melewati header
    data = [row for row in reader]

# Menambahkan kolom label
labels_count = {"C2": 0, "C1": 0, "B1": 0, "B2": 0, "A2": 0, "A1": 0}
new_data = []
for row in data:
    grade = int(row[2])  # Pastikan ini adalah kolom yang benar untuk grade
    label = label_grade(grade)
    labels_count[label] += 1
    new_row = row + [label]
    new_data.append(new_row)

# Menulis data baru ke file CSV
with open("new_data.csv", "w", newline="") as file:
    writer = csv.writer(file)
    writer.writerow(header + ["label"])  # Menulis header
    writer.writerows(new_data)  # Menulis baris data

# Mencetak total jumlah dari setiap label
for label, count in labels_count.items():
    print(f"Total {label}: {count}")
