import pandas as pd
import spacy
import numpy as np
import nltk

nltk.download("averaged_perceptron_tagger")
from nltk import pos_tag
from nltk.util import ngrams
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

# Baca file CSV
file_path = "new_data.csv"  # Ganti dengan path file CSV Anda
data = pd.read_csv(file_path)

# Ambil kolom "answer" dan "label"
answers = data["answer"].tolist()
label = data["label"].tolist()


# Fungsi untuk Ekstraksi Fitur
def extract_features(text):
    # Panjang Dokumen
    doc_length = len(text)

    # Word n-grams
    word_vectorizer = CountVectorizer(ngram_range=(1, 2))
    word_ngrams = word_vectorizer.fit_transform([text])

    # PoS n-grams
    pos_tags = pos_tag(text.split())
    pos_ngrams = list(ngrams([pos for word, pos in pos_tags], 2))

    # Dependency Triples (menggunakan spacy)
    nlp = spacy.load("en_core_web_sm")
    doc = nlp(text)
    dep_triples = [(token.text, token.dep_, token.head.text) for token in doc]

    # Word Embeddings (menggunakan spacy)
    word_embeddings = np.array([token.vector for token in doc])

    return doc_length, word_ngrams, pos_ngrams, dep_triples, word_embeddings


# Ekstrak fitur
feature_list = [extract_features(text) for text in answers]

# Simpan hasil ekstraksi fitur ke dalam DataFrame
data = {
    "Document Length": [feat[0] for feat in feature_list],
    "Word N-grams": [feat[1] for feat in feature_list],
    "PoS N-grams": [feat[2] for feat in feature_list],
    "Dependency Triples": [feat[3] for feat in feature_list],
    "Word Embeddings": [feat[4] for feat in feature_list],
    "Answer": answers,  # Menambahkan kolom "Answer"
    "Label": label  # Menambahkan kolom "Label"
}

df = pd.DataFrame(data)

# Export DataFrame ke file CSV
df.to_csv("hasil_ekstraksi_fitur.csv", index=False)
